var mongoose = require('mongoose');


var provClienteListarHistorialPedidos = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cedulaCliente: Number,
    nombreProducto: String,
    cantidad: Number,
    precioUnitario: Number,
    total: Number,
    nombreProveedor: String,
    numeroProveedor: String,
    estado: String,
    calificar: String


});
/*En el segundo string es la coleccion a la que yo quiero ingresar los datos.*/ 
module.exports = mongoose.model('PedidosCliente', provClienteListarHistorialPedidos, 'PedidosClientes');