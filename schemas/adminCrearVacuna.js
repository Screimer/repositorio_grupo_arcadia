var mongoose = require('mongoose');


var crearVacunasSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    animal: String,
});
module.exports = mongoose.model('Vacuna', crearVacunasSchema, 'Vacunas');