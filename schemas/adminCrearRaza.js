var mongoose = require('mongoose');


var crearRazasSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    animal: String,
});
module.exports = mongoose.model('Raza', crearRazasSchema, 'Razas');