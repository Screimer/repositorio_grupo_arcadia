var mongoose = require('mongoose');


var provFormRegProdyServSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    identificacionProveedor: String,
    nombreEmpresa:String,
    numeroTelEmpresa:String,
    nombreDelProducto: String,
    descripcion: String,
    precio: Number,
    especie: String,
    tipo: String,
    provincias:String,
    cantones:String,
    categoria: String,
    nombreFoto: String,
    path: String,

});
module.exports = mongoose.model('ProductoYServicio', provFormRegProdyServSchema, 'ProductosYServicios');