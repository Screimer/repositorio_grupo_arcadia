var mongoose = require('mongoose');


var crearAdministradorSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cuenta: String,
    email: String,
    contrasena: String,
});
module.exports = mongoose.model('Administrador', crearAdministradorSchema, 'Administradors');