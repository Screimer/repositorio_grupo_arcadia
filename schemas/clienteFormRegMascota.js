var mongoose = require('mongoose');


var crearMascotasSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    identificacion: String,
    name: String,
    raza: String,
    padecimientos: String,
    vacunas: String,
    nombreFoto: String,
    imagen:String,

});
module.exports = mongoose.model('Mascota', crearMascotasSchema, 'Mascotas');