var mongoose = require('mongoose');


var crearCategoriaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    tipo: String,
    animal: String,
    descripcion: String,

});
module.exports = mongoose.model('Categoria', crearCategoriaSchema, 'Categorias');