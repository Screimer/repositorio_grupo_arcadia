var mongoose = require('mongoose');


var crearProveedoresAceptadosSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cuenta:String,
    nombreEmpresa:String,
    tipoIdEmpresa: String,
    idEmpresa: String,
    numeroTelefonoEmpresa: String,
    provinciaEmpresa: String,
    cantonEmpresa: String,
    direccionEmpresa: String,
    nombreProveedor: String,
    primerApellido: String,
    segundoApellido: String,
    email: String,
    tipoIdProveedor: String,
    identificacionProveedor: String,
    numeroTelefonoProveedor: String,
    genero: String,
    fechaCumple: String,
    nombreFoto:String,
    contrasena:String,
});
module.exports = mongoose.model('Proveedor', crearProveedoresAceptadosSchema, 'Proveedores');