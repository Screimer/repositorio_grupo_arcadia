var mongoose = require('mongoose');


var crearPedidosClientesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombreEmpresaProducto:String,
    numeroIdentificacionProveedor:Number,
    nombreDelProducto:String,
    precio:Number,
    cantidadDeProducto:Number,
    subtotalDelProducto:Number,
    numeroTelefonoEmpresa:Number,
    numeroIdentificacionCliente:Number,
    nombreDelCliente:String,
    numeroDeTelefonoCliente:Number,
    estado:String

});
module.exports = mongoose.model('PedidoCliente', crearPedidosClientesSchema, 'PedidosClientes');