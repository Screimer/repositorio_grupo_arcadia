var mongoose = require('mongoose');

var indexSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cedulaCliente: String,
    nombreProducto: String,
    cantidad: String,
    precioUnitario: String,
    total: String,
    nombreProveedor: String,
    numeroProveedor: String,
    estado: String,
});
/*En el segundo string es la coleccion a la que yo quiero ingresar los datos.*/ 
module.exports = mongoose.model('Pedido', indexSchema, 'PedidosClientes');