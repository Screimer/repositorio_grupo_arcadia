var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var app = express();

mongoose.connect('mongodb+srv://Screimer:Heliosnxp1516@basedatos.dr8d7.mongodb.net/epet?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});


app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));


app.use('/categorias', require('./api/adminCrearCategorias.js'));
app.use('/vacunas', require('./api/adminCrearVacunas.js'));
app.use('/razas', require('./api/adminCrearRazas.js'));
app.use('/padecimientos', require('./api/adminCrearPadecimientos.js'));
app.use('/mascotas', require('./api/clienteFormRegMascotas.js'));
app.use('/clientes', require('./api/clienteFormRegClientes.js'));
app.use('/proveedoresPendientes', require('./api/proveedorRegProveedores.js'));
app.use('/administrador', require('./api/adminInformacions.js'));

//david
app.use('/html/proveedor/provFormRegProdyServ.html', require('./api/provFormRegProdsyServs.js'));
app.use('/ListaProductos' , require('./api/clienteListaProductosPorCategoria.js'));
app.use('/clienteDetalleProducto', require('./api/clienteDetalleProd.js'));
app.use('/proveedores', require('./api/adminListProv.js'));
app.use('/html/administrador/adminListProd.html' , require('./api/adminListProd'));
app.use('/html/administrador/adminListServ.html' , require('./api/adminListServ'));
app.use('/html/cliente/clienteListarHistorialPedidos.html' , require('./api/clienteListarHistorialPedidos'));

//Pablo
app.use('/pedidos', require('./api/provListPedidos.js'));
app.use('/proveedoresAceptados', require('./api/adminListProvAceptados.js'));



app.listen(8080, function(){
  console.log("Servidor arriba en 8080");
});
