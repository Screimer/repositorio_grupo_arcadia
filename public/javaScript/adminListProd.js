var filtrado = document.getElementById("filtrado");
let entrar = false;


function cargarTabla() {
    fetch("http://localhost:8080/html/administrador/adminListProd.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {
                for (var cont = 0; json.length > cont; cont++) {
                    if (json[cont].tipo == "Producto") {
                        entrar = true;
                        break;
                    }
                }
                if (entrar == true) {
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>"+ json[cont].especie + "</td><td>" + "₡" + json[cont].precio + "</td><td>"+ json[cont].categoria + "</td><td>" +"<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td>" 
                            document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                        }

                    }
                } else {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    document.getElementById("warning").innerHTML = "Sin Coincidencias";
                }


            }
        )
}


function cargarTablaFiltrada() {
    document.getElementById("warning").innerHTML = "";

    fetch("http://localhost:8080/html/administrador/adminListProd.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {

                if (filtrado.value == 1) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría"  + "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    entrar = false;
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            entrar = true;
                            break;
                        }
                    }
                    if(entrar == true){
                        var encontroEspecie = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].especie == "Gatos"){
                                encontroEspecie = true;
                                break;
                            }
                        }

                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroEspecie == true) {
                                if (json[cont].especie == "Gatos" && json[cont].tipo == "Producto") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><tr>"
                                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaProductosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }

                    }else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }
                    

                } else if (filtrado.value == 2) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría" + "</td><td>" + "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        entrar = false;
                        if (json[cont].especie == "Perros" && json[cont].tipo == "Producto" ) {
                            entrar = true;
                            break;
                        }
                    }

                    if (entrar == true) {
                        var encontroEspeciePerros = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].especie == "Perros"){
                                encontroEspeciePerros = true;
                                break;
                            }
                        }
                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroEspeciePerros == true) {
                                if (json[cont].especie == "Perros" && json[cont].tipo == "Producto") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><td>"  + "</td></tr>"
                                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaProductosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }
                    } else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                }else if (filtrado.value == 3) {
                    document.getElementById("tablaProductosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaProductosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría" + "</td>"+ "</table>"
                    document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Producto") {
                            entrar = true;
                            break;
                        }
                    }
                    if (entrar == true) {
                        for (var cont = 0; json.length > cont; cont++) {
                            if (json[cont].tipo == "Producto") {
                                var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><tr>"
                                document.getElementById("tablaProductosAdmin").insertAdjacentHTML("beforeend", linea);
                            }

                        }


                    } else {
                        document.getElementById("tablaProductosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                } 


            }
        )
}