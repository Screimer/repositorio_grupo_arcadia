function validacionFormulario() {
  var identificacion = document.getElementById("numeroIdentificacion");
  var respuestaFuncionNombre = validacionCampoNombre();
  var respuestaFuncionTipo = validacionSelectTipo();
  var respuestaFuncionApellidoDos = validacionCampoApellidoDos();
  var respuestaFuncionApellidoUno = validacionCampoApellidoUno();
  var respuestaFuncionGenero = validacionSelectGenero();
  var respuestaFuncionNumIden = validacionCampoNumIden();
  var respuestaFuncionFecha = validacionCampoFecha();
  var respuestaFuncionNumTel = validacionCampoNumTel();
  var respuestaFuncionDireccion = validacionCampoDireccion();
  var respuestaFuncionEmail = validarEmail();
  var g = validacionSubirImagen()
  var completo = 0;
  alertaIgual = "",
    duplicado = 0;
  const advIgual = document.getElementById("warningDuplicado");

  if (respuestaFuncionNombre == true && respuestaFuncionApellidoUno == true && respuestaFuncionApellidoDos == true && respuestaFuncionTipo == true && respuestaFuncionGenero == true && respuestaFuncionNumIden == true && respuestaFuncionFecha == true && respuestaFuncionNumTel == true && respuestaFuncionDireccion == true && respuestaFuncionEmail == true && g == true) {
    completo = 1;
    var pass = '';
    var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
      'abcdefghijklmnopqrstuvwxyz0123456789@#$';

    for (i = 1; i <= 8; i++) {
      var char = Math.floor(Math.random()
        * str.length + 1);

      pass += str.charAt(char)
    }

  };

  if (completo == 1) {
    var datos = {
      cuenta: "cliente",
      nombre: document.getElementById("nombreC").value,
      primerApellido: document.getElementById("primerApellido").value,
      segundoApellido: document.getElementById("segundoApellido").value,
      email: document.getElementById("email").value,
      genero: document.getElementById("genero").options[genero.selectedIndex].text,
      tipo: document.getElementById("tipoId").options[tipoId.selectedIndex].text,
      identificacion: document.getElementById("numeroIdentificacion").value,
      fecha: document.getElementById("fechaCumple").value,
      direccionExacta: document.getElementById("direccionExacta").value,
      numeroTelefono: document.getElementById("numeroTelefono").value,
      nombreFoto: document.getElementById("imagen").files[0].name,
      imagen: "public/imgs/fotosPerfilCliente/"+imagen.files[0].name,
      contrasena: pass,

    }

    fetch("http://localhost:8080/clientes")
      .then(
        function (response) {
          console.log(response);
          return response.json();
        }
      )
      .then(
        function (json) {
          for (var cont = 0; json.length > cont; cont++) {
            if (json[cont].identificacion == identificacion.value || json[cont].email == email.value) {
              duplicado = 1;
              ;
            }
          }
          if (duplicado == 1) {
            alertaIgual = `Ya existe una cuenta con este número de cédula y correo.`
            advIgual.innerHTML = alertaIgual
          } else {
            localStorage.setItem('datos', JSON.stringify(datos));
            console.log(localStorage.getItem('datos'));

            const formData = new FormData();

            for (let key in datos) {
              formData.append(key, datos[key]);
            }
            formData.append('imagen', imageFile);

            fetch("http://localhost:8080/clientes/subir", {
              method: 'POST',
              body: formData,
            })
              .then(
                function (response) {
                  return response.json();
                }
              ) 
            location.href = "http://localhost:8080/html/cliente/clienteFormRegMascotas.html"
          }
        }
      )

  }
}
function getCheckedCheckboxes(containerId) {
  return [...document.getElementById(containerId).childNodes].map(label =>
    label.getElementsByTagName('input')[0]).filter(input =>
      input.checked).map(n =>
        n.name).join(", ")
}

function validacionFormularioOtro() {
  var respuestaFuncionname = validacionCampoNombre();
  var respuestaFuncionTipo = validacionSelectTipo();
  var respuestaFuncionAnimal = validacionSelectAnimal();
  var g = validacionSubirImagen();
  var datosCliente = JSON.parse(localStorage.getItem('datos'));


  if (respuestaFuncionname == true && respuestaFuncionTipo == true && respuestaFuncionAnimal == true&&g==true) {
    completo = 1;
  };

  if (completo == 1) {
    var mascotas = {
      identificacion: datosCliente.identificacion,
      name: document.getElementById("nombre").value,
      raza: document.getElementById("tipo").options[tipo.selectedIndex].text,
      padecimientos: getCheckedCheckboxes("cargarPadecimientos"),
      vacunas: getCheckedCheckboxes("cargarVacunas"),
      nombreFoto: document.getElementById("imagen").files[0].name,
    }
    const formData = new FormData();

    for (let key in mascotas) {
      formData.append(key, mascotas[key]);
    }
    formData.append('imagen', imageFile);

    fetch("http://localhost:8080/mascotas/insertar", {
      method: 'POST',
      body: formData,
    })
      .then(
        function (response) {
          location.href = "http://localhost:8080/html/cliente/clienteFormRegMascotas.html"
          return response.json();
        }
      )
  }
}
function validacionFormularioU() {

  var respuestaFuncionname = validacionCampoNombre();
  var respuestaFuncionTipo = validacionSelectTipo();
  var respuestaFuncionAnimal = validacionSelectAnimal();
  var g = validacionSubirImagen()
  var datosCliente = JSON.parse(localStorage.getItem('datos'))


  if (respuestaFuncionname == true && respuestaFuncionTipo == true && respuestaFuncionAnimal == true&&g==true) {
    completo = 1;
  };

  if (completo == 1) {

  

    var datos = {
      cuenta: datosCliente.cuenta,
      nombre: datosCliente.nombre,
      primerApellido: datosCliente.primerApellido,
      segundoApellido: datosCliente.segundoApellido,
      email: datosCliente.email,
      genero: datosCliente.genero,
      tipo: datosCliente.tipo,
      identificacion: datosCliente.identificacion,
      fecha: datosCliente.fecha,
      direccionExacta: datosCliente.direccionExacta,
      numeroTelefono: datosCliente.numeroTelefono,
      nombreFoto:datosCliente.nombreFoto,
      imagen:datosCliente.imagen,
      contrasena: datosCliente.contrasena,
    }

    fetch("http://localhost:8080/clientes/insertar", {
      method: 'POST',
      body: JSON.stringify(datos),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(
        function (response) {
          return response.json();
        }
      )
    var mascotasDos = {
      identificacion: datosCliente.identificacion,
      name: document.getElementById("nombre").value,
      raza: document.getElementById("tipo").options[tipo.selectedIndex].text,
      padecimientos: getCheckedCheckboxes("cargarPadecimientos"),
      vacunas: getCheckedCheckboxes("cargarVacunas"),
      nombreFoto: document.getElementById("imagen").files[0].name,

    }
    const formData = new FormData();

    for (let key in mascotasDos) {
      formData.append(key, mascotasDos[key]);
    }
    formData.append('imagen', imageFile);

    fetch("http://localhost:8080/mascotas/insertar", {
      method: 'POST',
      body: formData,
    })
      .then(
        function (response) {
          return response.json();
        }
      )
      Email.send({
        Host: "smtp.gmail.com",
        Username: "epetstore1@gmail.com",
        Password: "Proyecto123",
        To: datosCliente.email,
        From: "epetstore1@gmail.com",
        Subject: "Creación de cuenta",
        Body: "Nos complace comentarle que su registro en e-pets se realizó con éxito. \nEsta sería su nueva contraseña:" + datosCliente.contrasena + ", la cual podrá modificar en la parte de 'He olvidado mi contraseña'. \nEsperemos que disfrute mucho nuestro servicio. \nEn este link podrá iniciar sesión: http://localhost:8080/html/universal/uniFormInicioSesion.html",
      }).then (
        function(response) { 
          console.log(response);
          location.href = "http://localhost:8080/html/cliente/clienteMensajeRegistro.html";
      }
      );
  }
}


