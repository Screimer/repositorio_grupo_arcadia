var filtrado = document.getElementById("filtrado");
function cargarTabla() {
    fetch("http://localhost:8080/html/administrador/adminListServ.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {
                var entrar = false;
                for (var cont = 0; json.length > cont; cont++) {
                    if (json[cont].tipo == "Servicio") {
                        entrar = true;
                        break;
                    }
                }
                if (entrar == true) {
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Servicio") {
                            var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>"+json[cont].especie+"</td><td>" + "₡"+ json[cont].precio + "</td><td>"  + json[cont].categoria + "</td><td>" +"<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td>" 
                            document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforeend", linea);
                        }

                    }
                } else {
                    document.getElementById("tablaServiciosAdmin").innerHTML = "";
                    document.getElementById("warning").innerHTML = "Sin Coincidencias";
                }


            }
        )
}

function cargarTablaFiltrada() {
    document.getElementById("warning").innerHTML = "";

    fetch("http://localhost:8080/html/administrador/adminListServ.html/")
        .then(
            function (response) {
                return response.json();

            }
        )
        .then(
            function (json) {

                if (filtrado.value == 1) {
                    document.getElementById("tablaServiciosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaServiciosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría" + "</td><td>" + "</table>"
                    document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    entrar = false;
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Servicio") {
                            entrar = true;
                            break;
                        }
                    }
                    if(entrar == true){
                        var encontroEspecieGatos = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].especie == "Gatos"){
                                encontroEspecieGatos = true;
                                break;
                            }
                        }

                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroEspecieGatos == true) {
                                if (json[cont].especie == "Gatos" && json[cont].tipo == "Servicio") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td></tr>"
                                    document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaServiciosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }

                    }else {
                        document.getElementById("tablaServiciosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }
                    

                } else if (filtrado.value == 2) {
                    document.getElementById("tablaServiciosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaServiciosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría" + "</td>" + "</table>"
                    document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        entrar = false;
                        if (json[cont].tipo == "Servicio" && json[cont].especie == "Perros" ) {
                            entrar = true;
                            break;
                        }
                    }

                    if (entrar == true) {
                        var encontroEspeciePerros = false;
                        for (var cont = 0; json.length > cont; cont++) {
                            if(json[cont].especie == "Perros"){
                                encontroEspeciePerros = true;
                                break;
                            }
                        }
                        for (var cont = 0; json.length > cont; cont++) {
                            if (encontroEspeciePerros == true) {
                                if (json[cont].especie == "Perros" && json[cont].tipo == "Servicio") {
                                    var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><tr>"
                                    document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforeend", linea);
                                } 
                            } else {
                                document.getElementById("tablaServiciosAdmin").innerHTML = "";
                                document.getElementById("warning").innerHTML = "Sin Coincidencias";
                            }


                        }
                    } else {
                        document.getElementById("tablaServiciosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                } else if (filtrado.value == 3) {
                    document.getElementById("tablaServiciosAdmin").innerHTML = "";
                    var headerTabla = '<table id="tablaServiciosAdmin" class="tabla">' + '<tr class="titulos"><td>' + "Nombre Empresa" + "</td><td>" + "# de identificación encargado" + "</td><td>" + "Nombre producto" + "</td><td>" + "Animal" + "</td><td>" + "Fotografía" + "</td><td>" + "Precio" + "</td><td>" + "Categoría" + "</td>"+ "</table>"
                    document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforebegin", headerTabla);
                    for (var cont = 0; json.length > cont; cont++) {
                        if (json[cont].tipo == "Servicio") {
                            entrar = true;
                            break;
                        }
                    }
                    if (entrar == true) {
                        for (var cont = 0; json.length > cont; cont++) {
                            if (json[cont].tipo == "Servicio") {
                                var linea = '<tr class="publicidad1"><td>' + json[cont].nombreEmpresa + "</td><td>" + json[cont].identificacionProveedor + "</td><td>" + json[cont].nombreDelProducto + "</td><td>" + json[cont].especie + "</td><td>" + "<a href ='#'>" + json[cont].nombreFoto + "</a>" + "</td><td>" + "₡" + json[cont].precio + "</td><td>" + json[cont].categoria + "</td><tr>"
                                document.getElementById("tablaServiciosAdmin").insertAdjacentHTML("beforeend", linea);
                            }

                        }


                    } else {
                        document.getElementById("tablaServiciosAdmin").innerHTML = "";
                        document.getElementById("warning").innerHTML = "Sin Coincidencias";
                    }

                }


            }
        )
}